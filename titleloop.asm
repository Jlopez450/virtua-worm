title:
		move.w #$8238,(a3)
		lea (titlescreen),a5
		lea (decloc),a0
		bsr decompress	
		lea (decloc),a5		
		move.l #$60000000,(a3)
		move.w #$4600,d4
		bsr vram_loop
		bsr generate_map
		lea (titletext),a5
		move.l #$0000c300,d5
		bsr draw_dialogue
		lea (titlepalette),a5
		move.l #$C0000000,(a3)
		move.w #$1f,d4
		bsr vram_loop
		
		lea (titlescroll),a5
		move.l #$0000ED00,d5
		bsr draw_dialogue	
	
		lea (titletheme)+66,a6
		move.l a6,VGM_start
		move.b #$ff,titleflag
		move.w #$2300, sr       ;enable ints			
titleloop:
		bsr read_controller
		cmpi.b #$7f,d3
		beq end_title
		bra titleloop
	
end_title:
		move.w #$2700, sr       ;disable ints	
		move.b #$00,titleflag
		move.w #$8134,(a3)
		move.w #$8228,(a3)
		move.l #$60000000,(a3)
		move.l #$6000,d4
		bsr vram_clear_loop
		move.l #$70000003,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$8174,(a3)		
		rts
		
VB_title:		;vector
		sub.w #$01,d1
		move.l #$70000003,(a3)
		move.w d1,(a4)
		bsr music_driver
		rte
		
draw_dialogue:
		move.l d5,d0
		bsr calc_vram
		move.l d0,(a3)
dialogueloop:	
		move.b (a5)+,d4		
		cmpi.b #$24,d4	;$ (end of line flag)
		beq nextline		
		cmpi.b #$25,d4	;% (end of block flag)
		beq return	
		andi.w #$00ff,d4
		eor.w #$a000,d4
        move.w d4,(a4)		
		bra dialogueloop
nextline:
		add.l #$00000080,d5 ;skip to next line
		bra draw_dialogue
		
generate_map:                  ;generate a map for 320x200 images
        move.l #$0100,d0         ;first tile
        move.w #$001c,d5
        move.l #$40000003,(a3) ;vram write to $c000	   
superloop:
        move.w #$27,d4
maploop:
        move.w d0,(a4)
        add.w #$1,d0
        dbf d4,maploop
        move.w #$17,d4
maploop2:
        move.w #$00,(a4)
        dbf d4,maploop2
        dbf d5,superloop
        rts
