newstage:	
		lea (stages),a0
		moveq #$00000000,d0
		move.b stage,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		jmp (a0)

stages:
 dc.w ld_stage1

ld_stage1:	
		lea (palette),a5
		move.l #$C0000000,(a3)
		move.w #$0f,d4
		bsr vram_loop	
		move.b #$04,direction
		move.w #$0123,totaldots
		move.w #$0002,playerX; in tiles
		move.w #$0002,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level1),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music1)+66,a6
		move.l a6,VGM_start		
		rts
		