VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ;Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8228	;field A    
	dc.w $8300	;$833e	
	dc.w $8406	;field B	
	dc.w $857c	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8AFF		
	dc.w $8B00		
	dc.w $8C81	
	dc.w $8D3C		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200
	
palette:
	dc.w $0000,$0CCC,$0008,$000E,$0282,$02c2,$02E2,$0E00
	dc.w $0000,$0E44,$00E0,$0E66,$0E88,$0ECC,$0000,$00E0
	
titlepalette:
	dc.w $0000,$04E4,$0AEA,$0CEC,$0EEE,$06E8,$0AA6,$0A00
	dc.w $0A66,$0C00,$0A88,$0600,$0C68,$0E66,$0C88,$0C40
	
titletext:
 dc.b " Press START to play $"
 dc.b " How to play:$"
 dc.b " $"
 dc.b " %"
hud:
 dc.b "             "
 dc.b "Virtua Worm  "
 dc.b "             "
 dc.b "Time- XX:XX  "
 dc.b "             "
 dc.b "Dots-XXX/XXX "
 dc.b "             "
 dc.b "Level- XX    "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "  mode5.net  "
 dc.b "            %"
 
;Playfield = 26x26 + 1 tile for CRT crop
;W = vert wall
;w = horiz wall
;X = + junction
;ABCD = bends starting top left clockwise
	
level1:
 dc.b "                                                                "	;crop
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB                                     "
 dc.b " WF F F F  F F F F F F F F                                      "
 dc.b " WF F F F W F F F F F F F W                                     "
 dc.b " WF F F F WF F F F F F F FW                                     "
 dc.b " WF F F F W F F F F F F F W                                     "
 dc.b " WF F F F WF F F F F F F FW                                     "
 dc.b " WF F F F W F F F F F F F W                                     "
 dc.b " WF F F F WF F F F F F F FW                                     "
 dc.b " WF F F F W F F F F F F F W                                     "
 dc.b " WF F F F W wwwwwwwwwwwww W                                     "
 dc.b " WF F F F W               W                                     "
 dc.b " WF F F F W AwwB WFFFFFFFFW                                     "
 dc.b " WF F F F W WXXW W        W                                     "
 dc.b " WF F F F W WXXW WFFFFFFFFW                                     "
 dc.b " WF F F F W DwwC W        W                                     "
 dc.b " WF F F F        WFFFFFFFFW                                     "
 dc.b " W wwwwwwwwwwwww W        W                                     "
 dc.b " WFFFFFFFFFFFFF FWFFFFFFFFW                                     "
 dc.b " WF           F FW        W                                     "
 dc.b " WF FFFFFFFFF F FWFFFFFFFFW                                     "
 dc.b " WF F       F F FW        W                                     "
 dc.b " WF F         F FWFFFFFFFFW                                     "
 dc.b " WF FFFFFFFFFFF FW        W                                     "
 dc.b " WF             FWFFFFFFFFW                                     "
 dc.b " WFFFFFFFFFFFFFFF         W                                     "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC                                     "
 dc.b "                                                               %"	;crop
 
font:
 incbin "gfx/ascii.bin"
 incbin "gfx/hexascii.bin"
 incbin "gfx/tiles.bin"
 
titlescreen:
 incbin "gfx/titlescreen.bin"
 
titletheme:
 incbin "music/title.vgm"
music1:
 incbin "music/music1.vgm"
 
