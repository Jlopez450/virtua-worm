    include "ramdat.asm"
    include "header.asm"

start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700, sr       ;disable ints
		bsr clear_regs
		bsr clear_ram
        bsr setup_vdp
		bsr clear_vram
		
		move.l #$40000000,(a3)
		lea (font),a5
		move.w #$BFF,d4
		bsr vram_loop
	
		move.l #$C0000000,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0eee,(a4)
	
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud
				
		lea (level1),a5
		bsr build_stage
		
        move.w #$2300, sr       ;enable ints		
		
loop:
		bra loop

setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        move.w (a5)+,(a4)
        dbf d4,vram_Loop
        rts  
clear_regs:
		moveq #$00000000,d0
		move.l d0,d1
		move.l d0,d2
		move.l d0,d3
		move.l d0,d4
		move.l d0,d5
		move.l d0,d6
		move.l d0,d7
		move.l d0,a0
		move.l d0,a1
		move.l d0,a2
		move.l d0,a3
		move.l d0,a4
		move.l d0,a5
		move.l d0,a6
		rts
		
clear_ram:
		move.w #$7FF0,d0
		move.l #$FF0000,a0	
clear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, clear_ram_loop
		rts
clear_vram:
		move.l #$40000000,(a3)
		move.w #$7fff,d4
vram_clear_loop:
		move.w #$0000,(a4)
		dbf d4, vram_clear_loop
		rts		
smalltextloop:
		move.w #$0027,d4		;draw 40 cells of text
		clr d5	
textloop:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return			 
		andi.w #$00ff,d5
		;or.w #$4000,d5
        move.w d5,(a4)	
		dbf d4,textloop	
		move.w #$0017,d4	    ;draw 24 cells of nothing
emptyloop:		
		move.w #$0000,(a4)
		dbf d4,emptyloop
		bra smalltextloop
		
build_hud:
		move.w #$000c,d4		;draw 12 cells of text
		clr d5	
textloop2:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return		 
		andi.w #$00ff,d5
		;or.w #$4000,d5
        move.w d5,(a4)	
		dbf d4,textloop2	
		move.w #$0032,d4	    ;draw 52 cells of nothing
emptyloop2:		
		move.w #$0000,(a4)
		dbf d4,emptyloop2
		bra build_hud
		
build_stage:
		move.l #$0000a000,d3
build_stageloop:
		move.l d3,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$001a,d4		;draw 26 cells of text
		clr d5	
textloop3:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return		 
		andi.w #$00ff,d5
		;or.w #$4000,d5
        move.w d5,(a4)	
		dbf d4,textloop3	
		add.w #$80,d3
		bra build_stageloop
		
calc_vram:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts	
		
return:
		rts
returnint:
		rte
		
ErrorTrap:        
        bra ErrorTrap

HBlank:
        rte

VBlank:
        rte
		
	include "data.asm"

ROM_End:
              
              