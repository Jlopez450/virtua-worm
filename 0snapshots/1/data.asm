VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ;Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8228	;field A    
	dc.w $8300	;$833e	
	dc.w $8406	;field B	
	dc.w $8518	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8AFF		
	dc.w $8B00		
	dc.w $8C81	
	dc.w $8D34		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200
	
;Playfield = 26x26 + 1 tile for CRT crop
;w = straight wall

hud:
 dc.b "             "
 dc.b " Virtua Worm "
 dc.b "             "
 dc.b " Time- XX:XX "
 dc.b "             "
 dc.b " Dots- XX/XX "
 dc.b "             "
 dc.b " Level- XX   "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "  mode5.net  "
 dc.b "            %"
	
level1:
 dc.b "                           "	;crop
 dc.b " wwwwwwwwwwwwwwwwwwwwwwwwww"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " w                        w"
 dc.b " wwwwwwwwwwwwwwwwwwwwwwwwww"
 dc.b "                          %"	;crop
 
font:
 incbin "gfx/ascii.bin"
 incbin "gfx/hexascii.bin"
