    include "ramdat.asm"
    include "header.asm"

start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700, sr       ;disable ints
		bsr clear_regs
		bsr clear_ram
        bsr setup_vdp
		bsr clear_vram
		
		move.l #$40000000,(a3)
		lea (font),a5
		move.w #$FFF,d4
		bsr vram_loop
	
		lea (palette),a5
		move.l #$C0000000,(a3)
		move.w #$0f,d4
		bsr vram_loop
				
		; lea (sprite_buffer),a5
		; move.w #$0100,(a5)+
		; move.w #$0001,(a5)+
		; move.w #$00FC,(a5)+
		; move.w #$0100,(a5)+
		
		move.b #$04,direction
		move.w #$0002,playerX; in tiles
		move.w #$0002,playerY; in tiles
			
		lea (level1),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud
        move.w #$2300, sr       ;enable ints		
		
loop:
		bra loop

setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        move.w (a5)+,(a4)
        dbf d4,vram_Loop
        rts  
clear_regs:
		moveq #$00000000,d0
		move.l d0,d1
		move.l d0,d2
		move.l d0,d3
		move.l d0,d4
		move.l d0,d5
		move.l d0,d6
		move.l d0,d7
		move.l d0,a0
		move.l d0,a1
		move.l d0,a2
		move.l d0,a3
		move.l d0,a4
		move.l d0,a5
		move.l d0,a6
		rts
		
clear_ram:
		move.w #$7FF0,d0
		move.l #$FF0000,a0	
clear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, clear_ram_loop
		rts
clear_vram:
		move.l #$40000000,(a3)
		move.w #$7fff,d4
vram_clear_loop:
		move.w #$0000,(a4)
		dbf d4, vram_clear_loop
		rts		
smalltextloop:
		move.w #$0027,d4		;draw 40 cells of text
		clr d5	
textloop:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return			 
		andi.w #$00ff,d5
		;or.w #$4000,d5
        move.w d5,(a4)	
		dbf d4,textloop	
		move.w #$0017,d4	    ;draw 24 cells of nothing
emptyloop:		
		move.w #$0000,(a4)
		dbf d4,emptyloop
		bra smalltextloop
		
build_hud:
		move.l #$0000a036,d3
hudloop:
		move.w #$000c,d4		;draw 12 cells of text
		clr d5
		move.l d3,d0
		bsr calc_vram
		move.l d0,(a3)
textloop2:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return		 
		andi.w #$00ff,d5
		;or.w #$4000,d5
        move.w d5,(a4)	
		dbf d4,textloop2	
		add.l #$0080,d3
		bra hudloop
		
build_stage:
		lea (level_buffer),a6
build_stageloop:
		move.w #$001a,d4		;draw 26 cells of text
		clr d5	
textloop3:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return
		andi.w #$00ff,d5
        move.b d5,(a6)+			;collision
		add.w #$0080,d5			;get correct tile
		;or.w #$4000,d5
        move.w d5,(a4)
		bra build_stageloop
		
calc_vram:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts	
		
read_controller:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		rts	
		
update_sprites:
		lea (sprite_buffer),a5
		move.l #$78000003,(a3) ;vram write $F800 (sprite)
		move.w #$0008,d4	
sprite_loop:
		move.w (a5)+,(a4)
		dbf d4, sprite_loop
		rts	
		
return:
		rts
returnint:
		rte
		
ErrorTrap:        
        bra ErrorTrap

HBlank:
        rte

VBlank:
		bsr read_controller
		bsr test_input
		bsr move_player
		;bsr update_sprites
        rte
		
move_player:		
		add.b #$01,counter1
		cmpi.b #$07,counter1
		bne return
		move.b #$00,counter1
		cmpi.b #$01,direction
		beq moveup
		cmpi.b #$02,direction
		beq movedown
		cmpi.b #$03,direction
		beq moveleft
		cmpi.b #$04,direction
		beq moveright
		rts
		
moveup:	
		bsr render_segment
		sub.w #$0001,playery
		move.w #$00FB,d2
		bsr render_pleyer
		rts
movedown:
		bsr render_segment
		add.w #$0001,playery
		move.w #$00FD,d2
		bsr render_pleyer
		rts
moveleft:
		bsr render_segment
		sub.w #$0001,playerX
		move.w #$00FE,d2
		bsr render_pleyer
		rts
moveright:	
		moveq #$00000001,d2
		bsr collision
		cmpi.b #$0057,d5
		beq return
		
		bsr render_segment
		add.w #$0001,playerX	;todo: only do if get food
		move.w #$00FC,d2
		bsr render_pleyer
		rts
		
collision:
		moveq #$00000000,d0
		moveq #$00000000,d1
		move.w playerx,d0
		move.w playery,d1	
		;lsl.w #$02,d0
		lsl.w #$06,d1
		;add.l #$0080,d0
		;sub.l #$0080,d1
		add.w d2,d0
		add.w d1,d0 		;all in d0
		add.l #$FF2000,d0  	
		move.l d0,a0
		move.b (a0),d5
		rts
		
render_pleyer:
		moveq #$00000000,d0
		moveq #$00000000,d1
		move.w playerx,d0
		move.w playery,d1	
		lsl.w #$01,d0
		lsl.w #$07,d1
		add.l #$0080,d0
		sub.l #$0080,d1
		add.w d1,d0 		;all in d0
		add.l #$0000a000,d0  	
		bsr calc_vram
		move.l d0,(a3)
		move.w d2,(a4)
		rts
		
render_segment:
		moveq #$00000000,d0
		moveq #$00000000,d1
		move.w playerx,d0
		move.w playery,d1	
		lsl.w #$01,d0
		lsl.w #$07,d1		
		add.w #$0080,d0
		sub.w #$0080,d1
		add.w d1,d0 		;all in d0		
		add.l #$0000a000,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$00FF,(a4)
		rts

		
test_input:
		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb, d7
		beq left	
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7, d7
		beq right					
		cmpi.b #$Fe, d3
		beq up	
		cmpi.b #$Fd, d3
		beq down	
		rts
		
up:
		move.b #$01,direction
		lea (sprite_buffer)+4,a5
		move.w #$00fb,(a5)
		rts
down:
		move.b #$02,direction
		lea (sprite_buffer)+4,a5
		move.w #$00fd,(a5)
		rts
left:
		move.b #$03,direction
		lea (sprite_buffer)+4,a5
		move.w #$00fe,(a5)
		rts
right:
		move.b #$04,direction
		lea (sprite_buffer)+4,a5
		move.w #$00fc,(a5)
		rts
		
check_right:		
		moveq #$00000000,d0
		moveq #$00000000,d1
		lea (sprite_buffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (sprite_buffer+6),a5
		move.w (a5),d0   ;X pos is now in d0	
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0078,d0
		sub.w #$0078,d1
		lsl.w #$03,d1	;multiply 8
		lsr.w #$03,d0	;divide 8
		add.w d0,d1
		lea (level_buffer),a5
		add.l d1,a5
		move.b (a5),d5	
		rts
check_left:		
		moveq #$00000000,d0
		moveq #$00000000,d1
		lea (sprite_buffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (sprite_buffer+6),a5
		move.w (a5),d0   ;X pos is now in d0	
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0080,d0
		sub.w #$0078,d1
		lsl.w #$03,d1	;multiply 8
		lsr.w #$03,d0	;divide 8
		add.w d0,d1
		lea (level_buffer),a5
		add.l d1,a5
		move.b (a5),d5	
		rts
check_up:		
		moveq #$00000000,d0
		moveq #$00000000,d1
		lea (sprite_buffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (sprite_buffer+6),a5
		move.w (a5),d0   ;X pos is now in d0	
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0080,d0
		sub.w #$0080,d1
		lsl.w #$03,d1	;multiply 8
		lsr.w #$03,d0	;divide 8
		add.w d0,d1
		lea (level_buffer),a5
		add.l d1,a5
		move.b (a5),d5	
		rts
		
check_down:		
		moveq #$00000000,d0
		moveq #$00000000,d1
		lea (sprite_buffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (sprite_buffer+6),a5
		move.w (a5),d0   ;X pos is now in d0	
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0080,d0
		sub.w #$0078,d1
		lsl.w #$03,d1	;multiply 8
		lsr.w #$03,d0	;divide 8
		add.w d0,d1
		lea (level_buffer),a5
		add.l d1,a5
		move.b (a5),d5	
		rts

		
	include "music_driver_V2.asm"
	include "data.asm"

ROM_End:
              
              