VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ;Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8228	;field A    
	dc.w $8300	;$833e	
	dc.w $8406	;field B	
	dc.w $857c	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8AFF		
	dc.w $8B00		
	dc.w $8C81	
	dc.w $8D3C		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200
	
palette:
	dc.w $0EEE,$0CCC,$0008,$000E,$02C2,$04E4,$08E8,$0E00
	dc.w $0000,$0E44,$00E0,$0E66,$0E88,$0ECC,$0000,$0000
hud:
 dc.b "             "
 dc.b " Virtua Worm "
 dc.b "             "
 dc.b " Time- XX:XX "
 dc.b "             "
 dc.b " Dots- XX/XX "
 dc.b "             "
 dc.b " Level- XX   "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "  mode5.net  "
 dc.b "            %"
 
;Playfield = 26x26 + 1 tile for CRT crop
;W = vert wall
;w = horiz wall
;X = + junction
;ABCD = bends starting top left clockwise
	
level1:
 dc.b "                                                                "	;crop
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB                                     "
 dc.b " W     F       F          W                                     "
 dc.b " W                 F      W                                     "
 dc.b " W         FFF     F      W                                     "
 dc.b " W                 F      W                                     "
 dc.b " W                 F      W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                    wwwwW                                     "
 dc.b " W                        W                                     "
 dc.b " W                    wwW W                                     "
 dc.b " W  F       AwwB        W W                                     "
 dc.b " W  F       WXXW        W W                                     "
 dc.b " W  F       WXXW        W W                                     "
 dc.b " W  F       DwwC        W W                                     "
 dc.b " W  F             wwwwwww W                                     "
 dc.b " W                        W                                     "
 dc.b " W                wwwwwwwwW                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC                                     "
 dc.b "                                                               %"	;crop
 
 
font:
 incbin "gfx/ascii.bin"
 incbin "gfx/hexascii.bin"
 incbin "gfx/tiles.bin"
