newstage:	
		lea (stages),a0
		moveq #$00000000,d0
		move.b stage,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		jmp (a0)

stages: 
 dc.w ld_stage4 ;tiny easy box
 dc.w ld_stage11;medium box
 dc.w ld_stage6 ;lower box
 dc.w ld_stage12;sphere 
 dc.w ld_stage5 ;key shaped
 dc.w ld_stage10;clover 
 dc.w ld_stage15;Last one 
 dc.w ld_stage2 ;eatdots 
 dc.w ld_stage8 ;boxes 
 dc.w return	;null
 dc.w return	;for BCD
 dc.w return
 dc.w return
 dc.w return
 dc.w return
 dc.w ld_stage1 ;WAS boring 
 dc.w ld_stage9 ;houses 
 dc.w ld_stage13;hard
 dc.w ld_stage7 ;face
 dc.w ld_stage14;Squarefield
 dc.w ld_stage3 ;pac man
 dc.w ending
 dc.w start

ld_stage1:	
		lea (palette2),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$02,direction
		move.w #$0172,totaldots
		move.w #$0000,dots
		move.w #$0002,playerX; in tiles
		move.w #$000C,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level1),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music2)+66,a6
		move.l a6,VGM_start
		rts
		
ld_stage2:	
		lea (palette2),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$03,direction
		move.w #$0109,totaldots
		move.w #$0000,dots
		move.w #$0019,playerX; in tiles
		move.w #$0002,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level2),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music2)+66,a6
		move.l a6,VGM_start		
		rts
		
ld_stage3:	
		lea (palette3),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$03,direction
		move.w #$0204,totaldots
		move.w #$0000,dots
		move.w #$000f,playerX; in tiles
		move.w #$0014,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level3),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music1)+66,a6
		move.l a6,VGM_start		
		rts

ld_stage4:	
		lea (palette),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$03,direction
		move.w #$0019,totaldots
		move.w #$0000,dots
		move.w #$0012,playerX; in tiles
		move.w #$0010,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level4),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music3)+66,a6
		move.l a6,VGM_start		
		rts
		
ld_stage5:	
		lea (palette),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$03,direction
		move.w #$0164,totaldots
		move.w #$0000,dots
		move.w #$000E,playerX; in tiles
		move.w #$000E,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level5),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music3)+66,a6
		move.l a6,VGM_start		
		rts
		
ld_stage6:	
		lea (palette),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$02,direction
		move.w #$0174,totaldots
		move.w #$0000,dots
		move.w #$000E,playerX; in tiles
		move.w #$0003,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level6),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music3)+66,a6
		move.l a6,VGM_start		
		rts
		
ld_stage7:	
		lea (palette3),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$02,direction
		move.w #$0120,totaldots
		move.w #$0000,dots
		move.w #$000D,playerX; in tiles
		move.w #$0003,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level7),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music1)+66,a6
		move.l a6,VGM_start		
		rts
		
ld_stage8:	
		lea (palette2),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$01,direction
		move.w #$0183,totaldots
		move.w #$0000,dots
		move.w #$0019,playerX; in tiles
		move.w #$000B,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level8),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music2)+66,a6
		move.l a6,VGM_start		
		rts
	
ld_stage9:	
		lea (palette3),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$04,direction
		move.w #$0187,totaldots
		move.w #$0000,dots
		move.w #$0006,playerX; in tiles
		move.w #$0012,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level9),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music1)+66,a6
		move.l a6,VGM_start		
		rts
		
ld_stage10:	
		lea (palette2),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$02,direction
		move.w #$0094,totaldots
		move.w #$0000,dots
		move.w #$0019,playerX; in tiles
		move.w #$000B,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level10),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music2)+66,a6
		move.l a6,VGM_start		
		rts
		
ld_stage11:	
		lea (palette),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$01,direction
		move.w #$0072,totaldots
		move.w #$0000,dots
		move.w #$0014,playerX; in tiles
		move.w #$000F,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level11),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music3)+66,a6
		move.l a6,VGM_start		
		rts
		
ld_stage12:	
		lea (palette),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$02,direction
		move.w #$0176,totaldots
		move.w #$0000,dots
		move.w #$0002,playerX; in tiles
		move.w #$0002,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level12),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music3)+66,a6
		move.l a6,VGM_start		
		rts
		
ld_stage13:	
		lea (palette3),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$03,direction
		move.w #$0215,totaldots
		move.w #$0000,dots
		move.w #$000D,playerX; in tiles
		move.w #$000F,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level13),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music1)+66,a6
		move.l a6,VGM_start		
		rts		
		
ld_stage14:	
		lea (palette3),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$04,direction
		move.w #$0216,totaldots
		move.w #$0000,dots
		move.w #$0002,playerX; in tiles
		move.w #$0002,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level14),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music1)+66,a6
		move.l a6,VGM_start		
		rts	
		
ld_stage15:	
		lea (palette2),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$02,direction
		move.w #$0212,totaldots
		move.w #$0000,dots
		move.w #$0002,playerX; in tiles
		move.w #$0003,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level15),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music2)+66,a6
		move.l a6,VGM_start		
		rts	
		
ending:	
		lea (palette),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$02,direction
		move.w #$0212,totaldots
		move.w #$0000,dots
		move.w #$0002,playerX; in tiles
		move.w #$0003,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (endingstage),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music2)+66,a6
		move.l a6,VGM_start		
		rts	