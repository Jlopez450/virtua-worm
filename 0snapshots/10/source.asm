    include "ramdat.asm"
    include "header.asm"

start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700, sr       ;disable ints
		move.l #$00000000,a7
 		move.w #$100,($A11100)
		move.w #$100,($A11200)  
		bsr clear_regs
		bsr clear_ram
        bsr setup_vdp
		bsr clear_vram
		bsr region_check
		
		move.l #$40000010,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		
		move.b #$01,stage
		move.l #$40000000,(a3)
		lea (font),a5
		move.w #$FFF,d4
		bsr vram_loop
	
		bsr title
		
		bsr newstage
		
		lea (background),a5
		; lea (decloc),a1
		; bsr decompress	
		; lea (decloc),a5	
		move.l #$60000000,(a3)
		move.w #$2a3F,d4
		bsr vram_loop
		bsr map_background
		
        move.w #$2300, sr       ;enable ints		
		
loop:
		move.b #$ff,vb_flag
		bsr music_driver
vb_wait:	
		tst.b vb_flag
		bne vb_wait
		bra loop

setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        move.w (a5)+,(a4)
        dbf d4,vram_Loop
        rts  
clear_regs:
		moveq #$00000000,d0
		move.l d0,d1
		move.l d0,d2
		move.l d0,d3
		move.l d0,d4
		move.l d0,d5
		move.l d0,d6
		move.l d0,d7
		move.l d0,a0
		move.l d0,a1
		move.l d0,a2
		move.l d0,a3
		move.l d0,a4
		move.l d0,a5
		move.l d0,a6
		rts
		
clear_ram:
		move.w #$7FF8,d0
		move.l #$FF0000,a0	
clear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, clear_ram_loop
		rts
clear_vram:
		move.l #$40000000,(a3)
		move.w #$7fff,d4
vram_clear_loop:
		move.w #$0000,(a4)
		dbf d4, vram_clear_loop
		rts		
smalltextloop:
		move.w #$0027,d4		;draw 40 cells of text
		clr d5	
textloop:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return			 
		andi.w #$00ff,d5
		;or.w #$4000,d5
        move.w d5,(a4)	
		dbf d4,textloop	
		move.w #$0017,d4	    ;draw 24 cells of nothing
emptyloop:		
		move.w #$0000,(a4)
		dbf d4,emptyloop
		bra smalltextloop
		
build_hud:
		move.l #$0000a036,d3
hudloop:
		move.w #$000c,d4		;draw 12 cells of text
		clr d5
		move.l d3,d0
		bsr calc_vram
		move.l d0,(a3)
textloop2:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return		 
		andi.w #$00ff,d5
		;or.w #$4000,d5
        move.w d5,(a4)	
		dbf d4,textloop2	
		add.l #$0080,d3
		bra hudloop
		
build_stage:
		lea (thunkfx),a6
		move.w #$0020,d4
		bsr load_sfx
		lea (level_buffer),a6
 		move.b #$9F,$c00011
		move.b #$EF,$c00011	;kill psg
        move.b #$FF,$c00011		
        move.b #$BF,$c00011	
		clr d7
build_stageloop:
		clr d5
textloop3:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return
		andi.w #$00ff,d5
        move.b d5,(a6)+			;collision
		add.w #$0080,d5			;get correct tile
		;or.w #$4000,d5
        move.w d5,(a4)
		
		add.b #$01,d7
		cmpi.b #$40,d7
		bge noise
noiseret:		
		move.w #$0800,d4
		bsr delay
		bra build_stageloop
delay:
		dbf d4, delay		
		rts
noise:
		bsr test2612
		move.b #$28,$A04000
		bsr test2612
		move.b #$06,$A04001
		bsr test2612
		move.b #$28,$A04000
		bsr test2612
		move.b #$f6,$A04001
		clr d7
		bra noiseret
		
calc_vram:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts	
calc_vram_read:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$0000,d0  ;attach vram read bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts	
		
read_controller:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		rts	
		
return:
		rts
returnint:
		rte
HBlank:
        rte

VBlank:
		tst titleflag
		bne VB_title
		bsr read_controller
		bsr test_input
		bsr move_player
		bsr draw_head
		bsr clock
		bsr update_hud
		clr vb_flag	
        rte
update_hud:
		move.l #$62c00002,(a3)
		moveq #$00000000,d0
		move.w dots,d0
		andi.w #$0F00,d0
		lsr.w #$08,d0
		add.w #$0030,d0
		move.w d0,(a4)
		move.w dots,d0
		andi.w #$00F0,d0
		lsr.w #$04,d0
		add.w #$0030,d0
		move.w d0,(a4)
		move.w dots,d0
		andi.w #$000F,d0
		add.w #$0030,d0
		move.w d0,(a4)
		
		move.l #$61c80002,(a3)		
		move.b seconds, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0		
		move.w d0, (a4)
		move.b seconds, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0		
		move.w d0, (a4)	
		
		move.l #$61c20002,(a3)	
		move.b minutes, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0	
		move.w d0, (a4)
		move.b minutes, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		move.w d0, (a4)	

		move.l #$63c40002,(a3)	
		move.b stage, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0	
		move.w d0, (a4)
		move.b stage, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		move.w d0, (a4)

		move.l #$62c80002,(a3)
		move.w totaldots,d0
		andi.w #$0F00,d0
		lsr.w #$08,d0
		add.w #$0030,d0
		move.w d0,(a4)
		move.w totaldots,d0
		andi.w #$00F0,d0
		lsr.w #$04,d0
		add.w #$0030,d0
		move.w d0,(a4)
		move.w totaldots,d0
		andi.w #$000F,d0
		add.w #$0030,d0
		move.w d0,(a4)		
		
		rts
		
		
draw_head:		
		moveq #$00000000,d0
		moveq #$00000000,d1
		move.w playerx,d0
		move.w playery,d1	
		lsl.w #$03,d0
		lsl.w #$03,d1	
		add.w #$0080,d1
		add.w #$0080,d0
		
		move.l #$78000003,(a3)			
		cmpi.b #$01,direction
		beq headup
		cmpi.b #$02,direction
		beq headdown
		cmpi.b #$03,direction
		beq headleft
		cmpi.b #$04,direction
		beq headright
		rts
		
headright:
		move.w d1,(a4)
		move.w #$0000,(a4)
		move.w #$00fc,(a4)
		move.w d0,(a4)
		rts
headleft:	
		move.w d1,(a4)
		move.w #$0000,(a4)
		move.w #$00fE,(a4)
		move.w d0,(a4)
		rts
headdown:
		move.w d1,(a4)
		move.w #$0000,(a4)
		move.w #$00fd,(a4)
		move.w d0,(a4)
		rts
headup:	
		move.w d1,(a4)
		move.w #$0000,(a4)
		move.w #$00fb,(a4)
		move.w d0,(a4)
		rts
			
move_player:		
		add.b #$01,counter1
		cmpi.b #$08,counter1	;cause of bad tight turns?
		bne return
		clr turn
		clr counter1
		cmpi.b #$01,direction
		beq moveup
		cmpi.b #$02,direction
		beq movedown
		cmpi.b #$03,direction
		beq moveleft
		cmpi.b #$04,direction
		beq moveright
		rts
		
moveup:	
		move.w #$FFC0,d2
		bsr collision
		cmpi.b #$0069,d5
		beq return
		cmpi.b #$0049,d5
		beq return
		cmpi.b #$0074,d5
		beq return
		cmpi.b #$0054,d5
		beq return	
		cmpi.b #$0057,d5
		beq return
		cmpi.b #$0058,d5
		beq return
		cmpi.b #$0077,d5
		beq return
		cmpi.b #$0041,d5
		beq return
		cmpi.b #$0042,d5
		beq return
		cmpi.b #$0043,d5
		beq return
		cmpi.b #$0044,d5
		beq return
		cmpi.b #$0046,d5 ;food
		beq up_food
		
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		sub.w #$0001,playery
		bsr check_selfhit		
		bsr remove_segment2
		bsr render_segment

		move.w #$00Ff,d2
		bsr render_pleyer
		rts
		
up_food:	
		move.b #$00,(a0)	;clear object from level buffer
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		sub.w #$0001,playery	
		;bsr remove_segment2
		bsr render_segment

		move.w #$00FF,d2
		bsr render_pleyer
		bsr add_dot
		rts
movedown:
		move.w #$0040,d2
		bsr collision
		cmpi.b #$0069,d5
		beq return
		cmpi.b #$0049,d5
		beq return
		cmpi.b #$0074,d5
		beq return
		cmpi.b #$0054,d5
		beq return	
		cmpi.b #$0057,d5
		beq return
		cmpi.b #$0058,d5
		beq return
		cmpi.b #$0077,d5
		beq return
		cmpi.b #$0041,d5
		beq return
		cmpi.b #$0042,d5
		beq return
		cmpi.b #$0043,d5
		beq return
		cmpi.b #$0044,d5
		beq return
		
		cmpi.b #$0046,d5 ;food
		beq down_food
		
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		add.w #$0001,playery
		bsr check_selfhit		
		bsr remove_segment2
		bsr render_segment

		move.w #$00Ff,d2
		bsr render_pleyer
		rts
down_food:	
		move.b #$00,(a0)
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		add.w #$0001,playery	
		;bsr remove_segment2
		bsr render_segment

		move.w #$00Ff,d2
		bsr render_pleyer
		bsr add_dot
		rts
		
moveleft:
		move.w #$FFFF,d2
		bsr collision
		cmpi.b #$0069,d5
		beq return
		cmpi.b #$0049,d5
		beq return
		cmpi.b #$0074,d5
		beq return
		cmpi.b #$0054,d5
		beq return	
		cmpi.b #$0057,d5
		beq return
		cmpi.b #$0058,d5
		beq return
		cmpi.b #$0077,d5
		beq return
		cmpi.b #$0041,d5
		beq return
		cmpi.b #$0042,d5
		beq return
		cmpi.b #$0043,d5
		beq return
		cmpi.b #$0044,d5
		beq return
		cmpi.b #$0046,d5 ;food
		beq left_food
		
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		sub.w #$0001,playerX
		bsr check_selfhit		
		bsr remove_segment2
		bsr render_segment

		move.w #$00Ff,d2
		bsr render_pleyer
		rts
		
left_food:	
		move.b #$00,(a0)
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		sub.w #$0001,playerX	
		bsr render_segment

		move.w #$00Ff,d2
		bsr render_pleyer
		bsr add_dot
		rts
		
moveright:	
		moveq #$00000001,d2
		bsr collision
		cmpi.b #$0069,d5
		beq return
		cmpi.b #$0049,d5
		beq return
		cmpi.b #$0074,d5
		beq return
		cmpi.b #$0054,d5
		beq return		
		cmpi.b #$0057,d5
		beq return
		cmpi.b #$0058,d5
		beq return
		cmpi.b #$0077,d5
		beq return
		cmpi.b #$0041,d5
		beq return
		cmpi.b #$0042,d5
		beq return
		cmpi.b #$0043,d5
		beq return
		cmpi.b #$0044,d5 ;walls and wall accessories
		beq return
	
		cmpi.b #$0046,d5 ;food
		beq right_food
		
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		add.w #$0001,playerX	
		bsr check_selfhit
		bsr remove_segment2
		bsr render_segment
		move.w #$00FF,d2
		bsr render_pleyer
		rts
		
right_food:	
		move.b #$00,(a0)
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		add.w #$0001,playerX	
		;bsr remove_segment2
		bsr render_segment

		move.w #$00FF,d2
		bsr render_pleyer
		bsr add_dot
		rts
		
;idea: stack based last. Constantly push new co-ordinates into memory.
;A1 for new spaces, another A2 for next to be cleared?
		
		
collision:
		moveq #$00000000,d0
		moveq #$00000000,d1
		move.w playerx,d0
		move.w playery,d1	
		lsl.w #$06,d1
		add.w d2,d0
		add.w d1,d0 		;all in d0
		add.l #level_buffer,d0  	
		move.l d0,a0
		move.b (a0),d5
		rts
		
check_selfhit:		
		moveq #$00000000,d0
		moveq #$00000000,d1
		move.w playerx,d0
		move.w playery,d1	
		lsl.w #$01,d0
		lsl.w #$07,d1
		add.l #$0080,d0
		sub.l #$0080,d1
		add.w d1,d0 		;all in d0
		add.l #$0000a000,d0  
		bsr calc_vram_read		
		move.l d0,(a3)
		move.w (a4),d0
		cmpi.w #$00FF,d0
		beq kill
		rts
		
kill:
 		move.b #$9F,$c00011
		move.b #$EF,$c00011	;kill psg
        move.b #$FF,$c00011		
        move.b #$BF,$c00011	
		lea (killtext),a5
		move.l #$0000a602,d5
		bsr draw_dialogue
killloop:
		bsr read_controller
		cmpi.b #$7f,d3
		beq retry	;stack growth?
		bra killloop
retry:		
		bsr newstage
		add.l #$08,a7
		rte
		
render_pleyer:
		moveq #$00000000,d0
		moveq #$00000000,d1
		move.w playerx,d0
		move.w playery,d1	
		lsl.w #$01,d0
		lsl.w #$07,d1
		add.l #$0080,d0
		sub.l #$0080,d1
		add.w d1,d0 		;all in d0
		add.l #$0000a000,d0  	
		bsr calc_vram
		move.l d0,(a3)
		move.w d2,(a4)
		rts
		
render_segment:
		moveq #$00000000,d0
		moveq #$00000000,d1
		move.w playerx,d0
		move.w playery,d1			
		lsl.w #$01,d0
		lsl.w #$07,d1		
		add.w #$0080,d0
		sub.w #$0080,d1
		add.w d1,d0 		;all in d0		
		add.l #$0000a000,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$00FF,(a4)
		rts
		
remove_segment:
		moveq #$00000000,d0
		moveq #$00000000,d1	
		move.w -(a1),d0	;x
		move.w -(a1),d1	;y	
		lsl.w #$01,d0
		lsl.w #$07,d1		
		add.w #$0080,d0
		sub.w #$0080,d1
		add.w d1,d0 		;all in d0		
		add.l #$0000a000,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)
		rts
remove_segment2:
		moveq #$00000000,d0
		moveq #$00000000,d1	
		move.w (a2)+,d1	;y			
		move.w (a2)+,d0	;x
		lsl.w #$01,d0
		lsl.w #$07,d1		
		add.w #$0080,d0
		sub.w #$0080,d1
		add.w d1,d0 		;all in d0		
		add.l #$0000a000,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)
		rts
		
add_dot:
		move.w #$01,d0
		move.w dots,d1
		cmpi.b #$99,d1
		beq centi
		abcd d0,d1
		move.w d1,dots
		move.w totaldots,d0
		cmp.w d0,d1
		beq levelup
		rts
centi:
		add.w #$0067,d1
		move.w d1,dots
		rts
levelup:
		bsr draw_head	;fix for misplaced head after win
		move.w #$0001,d0
		move.b stage,d1
		abcd d1,d0
		move.b d0,stage
		bra newstage
		
test_input:
		move.b d3,d7
		or.b #$bf,d7
		cmpi.b #$bf, d7	;a
		beq levelup
		; move.b d3,d7
		; or.b #$bf,d7
		; cmpi.b #$bf, d7	;a
		; bne noturbo
		; move.b #$07, counter1
; noturbo:
		tst turn
		bne return
		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb, d7
		beq left	
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7, d7
		beq right					
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe, d7
		beq up	
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd, d7
		beq down	
		rts

		
up:
		cmpi.b #$02,direction
		beq return
		clr counter1	;greatly improves quick turns
		move.b #$01,direction
		move.b #$ff,turn
		rts
down:
		cmpi.b #$01,direction
		beq return
		clr counter1
		move.b #$02,direction
		move.b #$ff,turn
		rts
left:
		cmpi.b #$04,direction
		beq return
		clr counter1
		move.b #$03,direction
		move.b #$ff,turn
		rts
right:
		cmpi.b #$03,direction
		beq return
		clr counter1
		move.b #$04,direction
		move.b #$ff,turn
		rts
		
map_background:
        move.l #$4100,d0         ;first tile
        move.w #$001c,d5
        move.l #$40820003,(a3) ;vram write to $c082	   
superloop2:
        move.w #$19,d4
maploop3:
        move.w d0,(a4)
        add.w #$1,d0
        dbf d4,maploop3
        move.w #$25,d4
maploop4:
        move.w #$00,(a4)
        dbf d4,maploop4
        dbf d5,superloop2
        rts
		
		
	include "titleloop.asm"
	include "clock.asm"
	include "stages.asm"
	include "music_driver_V2.asm"
	include "crashscreen.asm"
	include "decompress.asm"
	include "data.asm"

ROM_End:
              
              