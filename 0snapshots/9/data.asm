VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ;Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8228	;field A    
	dc.w $8300	;$833e	
	dc.w $8406	;field B	
	dc.w $857c	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8AFF		
	dc.w $8B00		
	dc.w $8C81	
	dc.w $8D3C		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200
	
palette:
	dc.w $0000,$0CCC,$0008,$000E,$0282,$02c2,$02E2,$0E00
	dc.w $0000,$0E44,$00E0,$0E66,$0E88,$0ECC,$0000,$00E0 ;fg
	
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0EEE ;txt

	dc.w $0000,$0002,$0004,$0000,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000 ;bg

	
titlepalette:
	dc.w $0000,$04E4,$0AEA,$0CEC,$0EEE,$06E8,$0AA6,$0A00
	dc.w $0A66,$0C00,$0A88,$0600,$0C68,$0E66,$0C88,$0C40
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0EEE
	
titletext:
 dc.b "          Press START to play$"
 dc.b " How to play:$"
 dc.b " $"
 dc.b " Use the D pad to steer yourself.$"
 dc.b " Each dot eaten will$" 
 dc.b " add a body segment.$"
 dc.b " Biting yourself is$" 
 dc.b " an instant death! $"
 dc.b " Collect all dots to $" 
 dc.b " reach the next stage.$"  
 dc.b "%"
 
killtext:
 dc.b "          Chomp!         $"
 dc.b "Press START to try again!%"
 
titlescroll:
 dc.b "Code, music, and graphics by James Lopez. ", $7f, "2016 Visit mode5.net  %"
 ;dc.b "Super early demo for /r/retrogaming    ", $7f, "2016 ComradeOj %"
hud:
 dc.b "             "
 dc.b "Virtua Worm  "
 dc.b "             "
 dc.b "Time- XX:XX  "
 dc.b "             "
 dc.b "Dots-XXX/XXX "
 dc.b "             "
 dc.b "Level- XX    "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "  mode5.net  "
 dc.b "            %"
 
;Playfield = 26x26 + 1 tile for CRT crop
;W = vert wall
;w = horiz wall
;X = + junction
;ABCD = bends starting top left clockwise
	
level1:
 dc.b "                                                                "	;crop
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB                                     "
 dc.b " WF F F F  F F F F F F F FW                                     "
 dc.b " WF F F F W F F F F F F F W                                     "
 dc.b " WF F F F WF F F F F F F FW                                     "
 dc.b " WF F F F W F F F F F F F W                                     "
 dc.b " WF F F F WF F F F F F F FW                                     "
 dc.b " WF F F F W F F F F F F F W                                     "
 dc.b " WF F F F WF F F F F F F FW                                     "
 dc.b " WF F F F W F F F F F F F W                                     "
 dc.b " WF F F F W wwwwwwwwwwwww W                                     "
 dc.b " WF F F F W               W                                     "
 dc.b " WF F F F W AwwB WFFFFFFFFW                                     "
 dc.b " WF F F F W WXXW W        W                                     "
 dc.b " WF F F F W WXXW WFFFFFFFFW                                     "
 dc.b " WF F F F W DwwC W        W                                     "
 dc.b " WF F F F        WFFFFFFFFW                                     "
 dc.b " W wwwwwwwwwwwww W        W                                     "
 dc.b " WFFFFFFFFFFFFF FWFFFFFFFFW                                     "
 dc.b " WF           F FW        W                                     "
 dc.b " WF FFFFFFFFF F FWFFFFFFFFW                                     "
 dc.b " WF F       F F FW        W                                     "
 dc.b " WF F         F FWFFFFFFFFW                                     "
 dc.b " WF FFFFFFFFFFF FW        W                                     "
 dc.b " WF             FWFFFFFFFFW                                     "
 dc.b " WFFFFFFFFFFFFFFF         W                                     "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC                                  %"
 
level2: 
 dc.b "                                                                "
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB                                     "
 dc.b " W                        W                                     "
 dc.b " W wwwBAwwwwBF AwwwB  AwwwI                                     "
 dc.b " WFFF WW FF W FW   W  W   W                                     "
 dc.b " WF   WW F FWF W   W  W   W                                     "
 dc.b " WFFF WW F FW FW   W  W   W                                     "
 dc.b " WF   WW F FWF W   W  W   W                                     "
 dc.b " WFFF WW FF W FW   W  W   W                                     "
 dc.b " W    WW    WF W   W  W   W                                     "
 dc.b " W F  WW  F W FW   W  W   W                                     "
 dc.b " WF F WW F FWF iwwwC  DwwwI                                     "
 dc.b " WFFF WW F FW FWFFFF  FFFFW                                     "
 dc.b " WF F WW F FWF WF  F  F  FW                                     "
 dc.b " WF F WW  F W FWF  F  F  FW                                     "
 dc.b " W    WW    WF WFFFF  FFFFW                                     "
 dc.b " WFFF WW FFFW FiwwwB  AwwwI                                     "
 dc.b " W F  WW  F WF W   W  W   W                                     "
 dc.b " W F  DC  F W FW   W  W   W                                     "
 dc.b " W F  FFF F WF W   W  W   W                                     "
 dc.b " W F F    F W FW   W  W   W                                     "
 dc.b " W    FF    WF W   W  W   W                                     "
 dc.b " W      F   W FW   W  W   W                                     "
 dc.b " W   FFF    WF W   W  W   W                                     "
 dc.b " W wwwwwwwwwC FDwwwC  DwwwI                                     "
 dc.b " W                        W                                     "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC                                  %"
 
 level3:
 dc.b "                                                                "
 dc.b " AwwwwwwwwwwwBAwwwwwwwwwwwB                                     "
 dc.b " WF F F F F FWWF F F F F FW                                     "
 dc.b " WFAwwBFAwwBFWWFAwwBFAwwBFW                                     "
 dc.b " WFDwwCFDwwCFWWFDwwCFDwwCFW                                     "
 dc.b " WFFFFFFFFFFFFFFFFFFFFFFFFW                                     "
 dc.b " WFAwwBF WFwwBAwwFW FAwwBFW                                     "
 dc.b " WFDwwCF WF  WW  FW FDwwCFW                                     "
 dc.b " WF    F WFFFWWFFFW F    FW                                     "
 dc.b " WF    F iww WW wwI F    FW                                     "
 dc.b " WF    F W        W F    FW                                     "
 dc.b " WFFFFFF W Aw  wB W FFFFFFW                                     "
 dc.b " DwwwwwF W W    W W FwwwwwC                                     "
 dc.b "       F   W    W   F                                           "
 dc.b "       F W DwwwwC W F                                           "
 dc.b " AwwwwwF W        W FwwwwwB                                     "
 dc.b " WFFFFFF W wwBAww W FFFFFFW                                     "
 dc.b " WF    FFFFFFWWFFFFFF    FW                                     "
 dc.b " WF    F    FWWF    F    FW                                     "
 dc.b " WF  wBF wwwFWWFwww FAw  FW                                     "
 dc.b " WFFFFWFFFFFFFFFFFFFFWFFFFW                                     "
 dc.b " iwwwFWF WFwwBAwwFW FWFwwwI                                     "
 dc.b " W   F F WF  WW  FW F F   W                                     "
 dc.b " WFFFFFF WFFFWWFFFW FFFFFFW                                     "
 dc.b " WFwwwwwwtwwFWWFwwtwwwwwwFW                                     "
 dc.b " WF F F F F F F F F F F F W                                     "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC                                  %"
 
level4: 
 dc.b "                                                                "
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W      AwwwwwwwwwwB      W                                     "
 dc.b " W      WF F F F F W      W                                     "
 dc.b " W      WFAwwwwwwB W      W                                     "
 dc.b " W      WFW      W W      W                                     "
 dc.b " W      WFW      W W      W                                     "
 dc.b " W      WFDwwwwwwCFW      W                                     "
 dc.b " W      WFFFFFFFFF W      W                                     "
 dc.b " W      DwwwwwwwwwwC      W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC                                  %"
 
level5: 
 dc.b "                                                                "
 dc.b " AwwwTwwwTwwTwwwwwwwwwwwwwB                                     " 
 dc.b " WFFFWFFFWFFW   FFFFFFFFFFW                                     "
 dc.b " WFWFWFWFWFFW   FAwwwwwwBFW                                     "
 dc.b " WFWFWFWFWFFW  WFW      WFW                                     "
 dc.b " WFWFWFWFWFFW  WFW      WFW                                     "
 dc.b " WFWFWFWFWFFW  WFW      WFW                                     "
 dc.b " WFWFWFWFWFFW  WFW      WFW                                     "
 dc.b " WFWFWFWFWFFW  WFW      WFW                                     "
 dc.b " WFWFWFWFWFFW  WFW      WFW                                     "
 dc.b " WFWFWFWFWFFW  WFDwwwwwwCFW                                     "
 dc.b " WFWFFFWFFFF   WFFFFFFFFFFW                                     " 
 dc.b " W DwwwtwwwwC  DwwwwwwwwwwI                                     "
 dc.b " W                        W                                     " 
 dc.b " W                        W                                     "
 dc.b " W wwwwwwwwwB  AwwwwwwwwwwI                                     " 
 dc.b " WFFFFFFFFFFW  W          W                                     "
 dc.b " WF        FW  W          W                                     "
 dc.b " WF        FW  W          W                                     "
 dc.b " WF        FDwwC          W                                     "
 dc.b " WF        FFFFFFFFFFFFFFFW                                     "
 dc.b " WF        FFFFFFFFFFFFFFFW                                     "
 dc.b " WF        FAwwB          W                                     "
 dc.b " WF        FW  W          W                                     "
 dc.b " WF        FW  W          W                                     "
 dc.b " WFFFFFFFFFFW  W          W                                     "
 dc.b " DwwwwwwwwwwC  DwwwwwwwwwwC                                  %"
 
level6:
 dc.b "                                                                "
 dc.b " AwwwwwwwwwwTwwTwwwwwwwwwwB                                     "
 dc.b " W          W  W          W                                     "
 dc.b " WFFFFFFFFFF    FFFFFFFFFFW                                     "
 dc.b " WF         W  W         FW                                     "
 dc.b " WF         W  W         FW                                     "
 dc.b " WF         W  W         FW                                     "
 dc.b " WF         W  W         FW                                     "
 dc.b " WF         W  W         FW                                     "
 dc.b " WF         W  W         FW                                     "
 dc.b " WF         W  W         FW                                     "
 dc.b " WF         W  W         FW                                     "
 dc.b " WFFFFFFFFFF    FFFFFFFFFFW                                     "
 dc.b " W          W  W          W                                     "
 dc.b " iwwwwwwwwwwC  DwwwwwwwwwwI                                     "
 dc.b " WFFFFFFFFFFFFFFFFFFFFFFFFW                                     "
 dc.b " WFAwwwwwwwwwwwwwwwwwwwwBFW                                     "
 dc.b " WFWFFFFFFFFFFFFFFFFFFFFWFW                                     "
 dc.b " WFWF                  FWFW                                     "
 dc.b " WFWF                  FWFW                                     "
 dc.b " WFWF                  FWFW                                     "
 dc.b " WFWF                  FWFW                                     "
 dc.b " WFDFwwwwwwwwwwwwwwwwwwFCFW                                     "
 dc.b " WFFFFFFFFFFFFFFFFFFFFFFFFW                                     "
 dc.b " WFwwwwwwwwwwwwwwwwwwwwwwFW                                     "
 dc.b " WFFFFFFFFFFFFFFFFFFFFFFFFW                                     "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC                                  %"
 
level7:
 dc.b "                                                                "
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB                                     "
 dc.b " W F F F F F F F F F F F FW                                     "
 dc.b " WFW AwwwwwwBF AwwwwwwB WFW                                     "
 dc.b " WFWFW      W FW      WFWFW                                     "
 dc.b " WFW W  AB  WF W  AB  W WFW                                     "
 dc.b " WFWFW  WW  W FW  WW  WFWFW                                     "
 dc.b " WFW W  WW  WF W  WW  W WFW                                     "
 dc.b " WFWFW  WW  W FW  WW  WFWFW                                     "
 dc.b " WFW W  WW  WF W  WW  W WFW                                     "
 dc.b " WFWFW  WW  W FW  WW  WFWFW                                     "
 dc.b " WFW W  WW  WF W  WW  W WFW                                     "
 dc.b " WFWFW  DC  W FW  DC  WFWFW                                     "
 dc.b " WFW W      WF W      W WFW                                     "
 dc.b " WFWFDwwwwwwC FDwwwwwwCFWFW                                     "
 dc.b " WFW F F F F FF F F F F WFW                                     "
 dc.b " WFWFAwTwwTwTwwTwTwwTwBFWFW                                     "
 dc.b " WFW W W  W W  W W  W W WFW                                     "
 dc.b " WFWFW W  W W  W W  W WFWFW                                     "
 dc.b " WFW W DwwC DwwC DwwC W WFW                                     "
 dc.b " WFWF F F F F F F F F F WFW                                     "
 dc.b " WFW W   AwwB  AwwB   W WFW                                     "
 dc.b " WFWFW   W  W  W  W   WFWFW                                     "
 dc.b " WFW W   W  W  W  W   W WFW                                     "
 dc.b " WFWFDwwwtwwtwwtwwtwwwCFWFW                                     "
 dc.b " WF F F F F F F F F F F F W                                     "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC                                  %"
 
level8:
 dc.b "                                                                "
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC                                  %"
 
template: 
 dc.b "                                                                "
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC                                  %"
 
teststage: 
 dc.b "                                                                "
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB                                     "
 dc.b " W FFFFFFFFFFFFFFFFFFFF   W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " W                        W                                     "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC                                  %"

 
font:
 incbin "gfx/ascii.bin"
 incbin "gfx/hexascii.bin"
 incbin "gfx/tiles.bin"
 
titlescreen:
 incbin "gfx/titlescreen.bin"
 
background:
 incbin "gfx/background2.bin"
 
thunkfx:
 incbin "music/thunk.fm"
 
titletheme:
 incbin "music/title.vgm"  ;title music
music1:
 incbin "music/music1.vgm" ;garbage blue
music2:
 incbin "music/music2.vgm" ;landmine green
music3:
 incbin "music/music3.vgm" ;freakshow red
